import sys
import os
from optparse import OptionParser
aFlaggedFile = None
bFlaggedFile = None

class Flag:
    no=0
    pos=1
    geno=2
    alt=4 # alt should be valued higher than geno
    
    

def get_chromosomes(vcfFn):
    chromosomes = []
    currChr="%"
    for line in open(vcfFn):
        if line[0:8]=="##contig":
            chromosomes.append(line.strip().split("ID=")[1].split(",")[0])
        elif line[0]!="#":
            break
    return chromosomes
        




def split_by_chromosomes(vcfFn):
    prevChr="%"
    header=""
    chros = get_chromosomes(vcfFn)
    fns = {}
    for c in chros:
        fileName = "%s.%s"%(vcfFn,c)
        if os.path.isfile(fileName):
            print fileName,"already exists"
            pass
        else:
            print fileName,"does not exist. Creating"
            fns[c]=open(fileName,"w")
    
    #early escape

    if len(fns)==0:
        print "All done already"
        return
    
    for line in open(vcfFn):
        if line[0]=="#":
            if "contig" not in line:
                for fh in fns:
                    fns[fh].write(line)
            else:
                for fh in fns:
                    if "ID=%s,"%fh in line:
                        fns[fh].write(line)
                        
        else:
            chro = line.strip().split("\t")[0]
            if chro in fns:
                fns[chro].write(line)
            
    for fh in fns:
        fns[fh].close()
            
                
def hash_from_fields(field):
    return "%s-%s-%s-%s"%(field[0],field[1],field[4],field[-1].split(":")[0])

def hash_from_line(line):
    field = line.strip().split("\t")
    return "%s-%s-%s-%s"%(field[0],field[1],field[4],field[-1].split(":")[0])

def pos_and_hash_from_line(line):
    lField = line.strip().split("\t")
    lAlt = lField[4]
    lGeno = lField[-1].split(":")[0]
    #lHash = hash_from_fields(lField)
    lChro = lField[0]
    lPos = int(lField[1])
    lHash = "%s-%s-%s-%s"%(lChro,lPos,lAlt,lGeno)
    return lPos, lAlt, lGeno,lHash
    

def valid_line(line):
    #print "Checkingvalidity:'",line,"'"
    return len(line.split("\t")) > 1

def get_hash(aHash, bHash):
    flag = 1 #pos always correct
    aAlt,aGeno = aHash.split("-")[2:4]
    bAlt,bGeno = bHash.split("-")[2:4]
    if aAlt == bAlt:
        flag |= Flag.alt
    if aGeno == bGeno:
        flag |= Flag.geno
        
    return flag
        
def create_hash_flag_dict(aHashes,bHashes):
    
    #check pos for debugging 
    #print ">>>>CREATE HASH FLAG DICT"
    aHashFlagMap = {}
    bHashFlagMap = {}
    #if len(aHashes)>1 or len(bHashes)>1:
        #print ">>>>MULTI",len(aHashes),"vs",len(bHashes)
    for aH in aHashes:
        bestAflag = 1
        for bH in bHashes:
            aFlag = get_hash(aH,bH)
            if aFlag > bestAflag:
                bestAflag = aFlag

        aHashFlagMap[aH]=bestAflag
        
    for bH in bHashes:
        bestBflag = 1
        for aH in aHashes:
            bFlag = get_hash(bH,aH)
            if bFlag > bestBflag:
                bestBflag = bFlag
        bHashFlagMap[bH]=bestBflag
  

        
    return aHashFlagMap, bHashFlagMap
        
        
        
def add_flag(line,num):
    field = line.split("\t")
    field[7]="%s;IntFl=%d"%(field[7],num)
    return '\t'.join(field)
    
def compare_vcfs(aVcfFn, bVcfFn): #single chromosome vcf
    print "Compare Vcfs", aVcfFn
    #step wise read lines from both files
    aVcf = open(aVcfFn)
    bVcf = open(bVcfFn)
    
    EOF=False #end of file
    aPos = 0
    bPos = 0
    
    aUniques = 0
    bUniques = 0
    matches = 0
    samePos =0
    eof=""

    
    aLine = aVcf.readline()
    while len(aLine)>0 and aLine[0]=="#":
        #aFlaggedFile.write(aLine)
        aLine = aVcf.readline()
    
    bLine = bVcf.readline()
    while len(bLine)>0 and bLine[0]=="#":
        #bFlaggedFile.write(bLine)
        bLine = bVcf.readline()
        
    #TODO: Seems to be cutting off first entry of file in .flagged file!
    #ensure there are lines available
    if not valid_line(aLine):
        while valid_line(bLine):
            bUniques+=1
            bLine = add_flag(bLine,Flag.no)
            #write as uniq
            bFlaggedFile.write(bLine) 
            bLine = bVcf.readline()
        aVcf.close()
        bVcf.close()
        return
    
    if not valid_line(bLine):
        while valid_line(aLine):
            aUniques+=1
            aLine = add_flag(aLine,Flag.no)
            #write as uniq
            aFlaggedFile.write(aLine)
            aLine = aVcf.readline()
        aVcf.close()
        bVcf.close()        
        return 
       
        
    #split first entries
    aPos,aAlt,aGeno,aHash = pos_and_hash_from_line(aLine)
    bPos,bAlt,bGeno,bHash = pos_and_hash_from_line(bLine)


    
    while True:
        if aPos==bPos:
            #positions the same, but genotypes or variant not the same
            #print "_______________"
            #print "SAME POS!"
            #print "A:",aLine
            #print "B:",bLine
            thisPos = aPos
            #print "Checking vs thisPos of",thisPos
            
            
            #create storage sets
            aHashes = set()
            bHashes = set()
            aHashes.add(aHash)
            bHashes.add(bHash)
            aLines = []
            bLines = []
            aHashLinesDict = {}
            bHashLinesDict = {}
            aHashLinesDict[aHash]=aLine
            bHashLinesDict[bHash]=bLine
            aLines.append(aLine)
            bLines.append(bLine)
            
            #grab all lines with the same pos 
            aLine = aVcf.readline()
            #print "Checking:",aLine
            while valid_line(aLine) and int(aLine.split("\t")[1])==thisPos:
                #print "adding"
                aPos,aAlt,aGeno,aHash = pos_and_hash_from_line(aLine)
                aHashes.add(aHash)
                aLines.append(aLine)
                aHashLinesDict[aHash]=aLine
                aLine = aVcf.readline()
            
            bLine = bVcf.readline()
            #print "Checking SECOND LINE!!!!:",bLine
            #print "valid?",valid_line(bLine)
            while valid_line(bLine) and int(bLine.split("\t")[1])==thisPos:
                #print "Adding"
                bPos,bAlt,bGeno,bHash = pos_and_hash_from_line(bLine)
                bHashes.add(bHash)
                bLines.append(bLine)
                bHashLinesDict[bHash]=bLine
                bLine = bVcf.readline()
                
            
            #print "AHASHES:",aHashes
            #print "BHASHES:",bHashes
            
            #do comparisons and generate flags for both A and B variants
            #print "#THIS POS:",thisPos
            #print "#A:",aHashes
            #print "#B:",bHashes
            aHashFlagDict,bHashFlagDict = create_hash_flag_dict(aHashes,bHashes)
            
            for a in aHashFlagDict:
                aF = aHashFlagDict[a]
                aH = a
                aL = add_flag(aHashLinesDict[a],aF)
                #print "###",aH,aL
                aFlaggedFile.write(aL)
            
            for b in bHashFlagDict:
                bF = bHashFlagDict[b]
                bH = b
                bL = add_flag(bHashLinesDict[b],bF)
                bFlaggedFile.write(bL)
                
            #compare hashes
            removeList =[] #store ones that need removing in here
            for aH in aHashes:
                if aH in bHashes:
                    matches+=1
                    removeList.append(aH)
            #remove matches from hash sets
            for r in removeList:
                aHashes.remove(r)
                bHashes.remove(r)
            
            
            #remove corresponding lines from line sets, and write them to matched files
            for i in xrange(len(aLines) - 1, -1, -1): #reverse iterate
                if hash_from_line(aLines[i]) in removeList:
                    #print "Removing line:",aLines[i]
                    aLines[i]=add_flag(aLines[i],Flag.pos|Flag.alt|Flag.geno)
                    #aFlaggedFile.write(aLines[i])
                    #aMatchFile.write(aLines[i]) #@@@WRITE
                    del aLines[i]
            for i in xrange(len(bLines) - 1, -1, -1): #reverse iterate
                if hash_from_line(bLines[i]) in removeList:
                    #print "Removing line:",bLines[i]
                    bLines[i]=add_flag(bLines[i],Flag.pos|Flag.alt|Flag.geno)
                    #bFlaggedFile.write(bLines[i])
                    #bMatchFile.write(bLines[i]) #@@@WRITE
                    del bLines[i]            
            
                    
            #print "Found",len(removeList),"perfect matches (",removeList,")"
            #print "AHASHES:",aHashes
            #print "BHASHES:",bHashes      
 
            #parse remainder out to uniq files TODO: Further divide by parsing to dif geno/same pos file or dif alt/same pos file
            for aL in aLines:
                aL = add_flag(aL,Flag.no)
                #aFlaggedFile.write(aL)
                #aUniqFile.write(aL) #@@@WRITE
            for bL in bLines:
                bL = add_flag(bL,Flag.no)
                #bFlaggedFile.write(bL)
                #bUniqFile.write(bL) #@@@WRITE
            
                    
                
            #add uniques to a and b
            aUniques+=len(aHashes)
            bUniques+=len(bHashes)
            
            #check eofs
            if(valid_line(aLine)):
                aPos,aAlt,aGeno,aHash = pos_and_hash_from_line(aLine)
            else:
                eof = "a"
                break
            if(valid_line(bLine)):
                bPos,bAlt,bGeno,bHash = pos_and_hash_from_line(bLine)
            else:
                eof = "b"
                break            
        else:
            #print "No match:",aHash,bHash
            if aPos < bPos:
                #shift along fileA
                aUniques+=1
                
                #write as uniq
                aLine = add_flag(aLine,Flag.no)
                aFlaggedFile.write(aLine)
                #aUniqFile.write(aLine) #@@@WRITE
                
                #print "a<b: iterating a"
                aLine = aVcf.readline()
                if valid_line(aLine):
                    aPos,aAlt,aGeno,aHash = pos_and_hash_from_line(aLine)
                else:
                    eof="a"
                    break
            elif aPos > bPos:
                #print "a>b: iterating b"
                bUniques+=1
                
                #write as uniq
                bLine = add_flag(bLine,Flag.no)
                bFlaggedFile.write(bLine)                
                #bUniqFile.write(bLine) #@@@WRITE
                
                bLine = bVcf.readline()
                if valid_line(bLine):
                    bPos,bAlt,bGeno,bHash = pos_and_hash_from_line(bLine)
                else:
                    eof="b"
                    break
            else:
                print "ERROR"

                
    #check which file ended and read the rest of the other
    #print "CHECKING EOF '",eof,"'"
    if eof=="a":
        if (valid_line(bLine)):
            bLine = add_flag(bLine,Flag.no)
            bFlaggedFile.write(bLine)
            bLine = bVcf.readline()
            while valid_line(bLine):
                bUniques+=1
                #write as uniq
                bLine = add_flag(bLine,Flag.no)
                bFlaggedFile.write(bLine)             
                #bUniqFile.write(bLine) #@@@WRITE  
                bLine = bVcf.readline()
    elif eof=="b":
        if (valid_line(aLine)):
            aLine = add_flag(aLine,Flag.no)
            aFlaggedFile.write(aLine)
            aLine = aVcf.readline()
            while valid_line(aLine):
                aUniques+=1
                #write as uniq
                aLine = add_flag(aLine,Flag.no)
                aFlaggedFile.write(aLine)             
                #aUniqFile.write(aLine) #@@@WRITE  
                aLine = aVcf.readline()
    #print "UNIQS: A:",aUniques,"\tB:",bUniques,"\tM:",matches,"\tS:",samePos
    aVcf.close()
    bVcf.close()


def open_output_files(outputDir, aFileName,bFileName):  
    global aFlaggedFile
    global bFlaggedFile
    aFlaggedFile = open("%s/%s.flagged"%(outputDir,os.path.split(aFileName)[-1]),"w")
    bFlaggedFile = open("%s/%s.flagged"%(outputDir,os.path.split(bFileName)[-1]),"w")
         
#MAIN   


#Parse command line options
parser = OptionParser()
parser.add_option("-a", "--vcfa", dest="vcf1Fn", action="store", help="Filename of first VCF file", metavar="STRING")
parser.add_option("-b", "--vcfb", dest="vcf2Fn", action="store", help="Filename of second VCF file", metavar="STRING")
parser.add_option("-o", "--outputDir", dest="outputDir", action="store", help="Output directory", metavar="STRING")
(options,args) = parser.parse_args()

vcf1Fn=options.vcf1Fn
vcf2Fn=options.vcf2Fn
outputDir = options.outputDir

#create output directory if needed
if os.path.exists(outputDir):
    if not os.path.isdir(outputDir):
        print "Error: Output directory already exists as a file (not a dir). Aborting"
        sys.exit()
else:
    os.makedirs(outputDir)
    
    


#grab chromosomes
aChromosomes = get_chromosomes(vcf1Fn)
bChromosomes = get_chromosomes(vcf2Fn)

#prepare output files
open_output_files(outputDir,vcf1Fn,vcf2Fn)

#get common chromosomes
commonChros = []
for aC in aChromosomes:
    if aC in bChromosomes:
        commonChros.append(aC)
print commonChros



#split by chromosome
print "Splitting %s"%vcf1Fn
split_by_chromosomes(vcf1Fn)
print "Splitting %s"%vcf2Fn
split_by_chromosomes(vcf2Fn)

#compare chromosomal vcfs
for chro in commonChros:   
    compare_vcfs("%s.%s"%(vcf1Fn,chro),"%s.%s"%(vcf2Fn,chro))
    
#clean up
aFlaggedFile.close()
bFlaggedFile.close()